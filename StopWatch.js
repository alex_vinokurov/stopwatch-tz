class StopWatch {
	constructor(container) {
		this._value = 0;
		container.innerHTML = '<canvas width="300px" height="300px" id="watch"></canvas>';
		this.canvas = document.getElementById('watch');
		this.renderWatch();
	}

	// Отрисовка таймера
	renderWatch() {
		const context = this.canvas.getContext('2d');
		const width = this.canvas.clientWidth;
		const height = this.canvas.clientHeight;
		const sec = 6 * Math.PI / 180;

		const position = this._value * sec;

		// Рисуем циферблат
		context.clearRect(0, 0, width, height);
		context.beginPath();
		context.arc(width / 2, height / 2, 100, 0, 2 * Math.PI);
		context.stroke();
		context.closePath();

		// Рисуем стрелку
		context.beginPath();
		context.arc(width / 2,
			height / 2,
			4, 0, 2 * Math.PI);
		context.fill();
		context.closePath();
		context.beginPath();
		context.moveTo(width / 2, height / 2);
		context.lineTo(width / 2 + 95 * Math.cos(position - Math.PI / 2),
			height / 2 + 95 * Math.sin(position - Math.PI / 2));
		context.stroke();
		context.closePath();

	}

	tick() {
		this.value = this.value + 1;
		this.renderWatch()
	}

	reset() {
		this.value = 0;
		this.renderWatch()
	}

	// Запуск таймера
	start() {
		if (!this.timerId) {
			this.timerId = setInterval(this.tick.bind(this), 1000);
		}
	}

	// Пауза таймера, value не сбрасывается
	pause() {
		clearInterval(this.timerId);
		this.timerId = 0;
	}

	// Остановка таймера, сброс value
	stop() {
		clearInterval(this.timerId);
		this.timerId = 0;
		this.reset();
	}

	// Геттер для value
	get value() {
		return this._value;
	}

	// Сеттер для value
	set value(value) {
		this._value = value;
	}
}
